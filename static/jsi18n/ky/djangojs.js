

(function(globals) {

  var django = globals.django || (globals.django = {});

  
  django.pluralidx = function(n) {
    var v=0;
    if (typeof(v) == 'boolean') {
      return v ? 1 : 0;
    } else {
      return v;
    }
  };
  

  /* gettext library */

  django.catalog = django.catalog || {};
  
  var newcatalog = {
    "%(sel)s of %(cnt)s selected": [
      "%(cnt)s\u043d\u0435\u0440\u0441\u0435\u0434\u0435\u043d %(sel)s\u0442\u0430\u043d\u0434\u0430\u043b\u0434\u044b"
    ],
    "6 a.m.": "\u0441\u0430\u0430\u0440\u043b\u0430\u043f \u0441\u0430\u0430\u0442 6",
    "6 p.m.": "\u041a\u044d\u0447 \u0441\u0430\u0430\u0442 6",
    "Add": "A\u00f1adir",
    "April": "\u0410\u043f\u0440\u0435\u043b\u044c",
    "August": "\u0410\u0432\u0433\u0443\u0441\u0442",
    "Available %s": "%s \u0434\u0430\u0430\u043d\u0430 \u0436\u0435\u0442\u043a\u0438\u043b\u0438\u043a\u0442\u04af\u04af",
    "Cancel": "\u0416\u043e\u043a\u043a\u043e \u0447\u044b\u0433\u0430\u0440",
    "Choose": "\u0422\u0430\u043d\u0434\u043e\u043e",
    "Choose a Date": "\u041a\u04af\u043d \u0442\u0430\u043d\u0434\u0430",
    "Choose a Time": "\u0422\u043e\u043b\u0443\u043a \u0443\u0431\u0430\u043a \u0442\u0430\u043d\u0434\u0430",
    "Choose a time": "\u041a\u044b\u0441\u043a\u0430 \u0443\u0431\u0430\u043a \u0442\u0430\u043d\u0434\u0430",
    "Choose all": "\u0411\u0430\u0430\u0440\u044b\u043d \u0442\u0430\u043d\u0434\u043e\u043e",
    "Chosen %s": "%s \u0434\u0430\u0430\u043d\u0430 \u0442\u0430\u043d\u0434\u0430\u043b\u0434\u044b",
    "Click to choose all %s at once.": "\u0411\u04af\u0442 %s \u0434\u0430\u0430\u043d\u0430\u043d\u044b \u0437\u0430\u043c\u0430\u0442\u0442\u0430 \u0442\u0430\u043d\u0434\u043e\u043e \u04af\u0447\u04af\u043d \u0447\u044b\u043a\u044b\u043b\u0434\u0430\u0442\u044b\u04a3\u044b\u0437.",
    "Click to remove all chosen %s at once.": "\u0422\u0430\u043d\u0434\u0430\u043b\u0433\u0430\u043d %s  \u0434\u0430\u0430\u043d\u0430\u043d\u044b\u043d \u0431\u0430\u0430\u0440\u044b\u043d \u04e9\u0447\u04af\u0440\u04af\u04af \u04af\u0447\u04af\u043d \u0431\u0430\u0441\u044b\u04a3\u044b\u0437",
    "December": "\u0414\u0435\u043a\u0430\u0431\u0440\u044c",
    "Delete": "Eliminar",
    "February": "\u0424\u0435\u0432\u0440\u0430\u043b\u044c",
    "Filter": "\u0427\u044b\u043f\u043a\u0430\u043b\u043e\u043e",
    "General": "General",
    "Hide": "\u0416\u0430\u0448\u044b\u0440",
    "January": "\u042f\u043d\u0432\u0430\u0440\u044c",
    "July": "\u0418\u044e\u043b\u044c",
    "June": "\u0418\u044e\u043d\u044c",
    "March": "\u041c\u0430\u0440\u0442",
    "May": "\u041c\u0430\u0439",
    "Midnight": "\u0422\u04af\u043d\u04af\u0447\u04af",
    "Noon": "\u0422\u04af\u0448",
    "Note: You are %s hour ahead of server time.": [
      "\u042d\u0441\u043a\u0435\u0440\u0442\u04af\u04af: \u0421\u0438\u0437 \u0441\u0435\u0440\u0432\u0435\u0440\u0434\u0435\u043d %s \u0441\u0430\u0430\u0442 \u0430\u043b\u0434\u044b\u0434\u0430 \u0436\u04af\u0440\u04e9\u0441\u04af\u0437."
    ],
    "Note: You are %s hour behind server time.": [
      "\u042d\u0441\u043a\u0435\u0440\u0442\u04af\u04af: \u0421\u0438\u0437 \u0441\u0435\u0440\u0432\u0435\u0440\u0434\u0435\u043d %s \u0441\u0430\u0430\u0442 \u0430\u0440\u043a\u0430\u0434\u0430 \u0436\u04af\u0440\u04e9\u0441\u04af\u0437."
    ],
    "November": "\u041d\u043e\u044f\u0431\u0440\u044c",
    "Now": "\u0410\u0437\u044b\u0440",
    "October": "\u041e\u043a\u0442\u044f\u0431\u0440\u044c",
    "Remove": "\u0410\u043b\u044b\u043f \u0442\u0430\u0448\u0442\u043e\u043e",
    "Remove all": "\u0411\u0430\u0430\u0440\u044b\u043d \u0430\u043b\u044b\u043f \u0442\u0430\u0448\u0442\u0430",
    "September": "\u0421\u0435\u043d\u0442\u044f\u0431\u0440\u044c",
    "Show": "\u041a\u04e9\u0440\u0441\u04e9\u0442",
    "This is the list of available %s. You may choose some by selecting them in the box below and then clicking the \"Choose\" arrow between the two boxes.": "\u0411\u0443\u043b \u0436\u0435\u0442\u043a\u0438\u043b\u0438\u043a\u0442\u04af\u04af \u0442\u0438\u0437\u043c\u0435\u0441\u0438 %s \u0434\u0430\u0430\u043d\u0430 . \u0421\u0438\u0437 \u0442\u04e9\u043c\u04e9\u043d\u0434\u04e9 \u043a\u0443\u0442\u0443\u0434\u0430\u043d \u043a\u044d\u044d \u0431\u0438\u0440\u043b\u0435\u0440\u0438\u043d \"\u0422\u0430\u043d\u0434\u043e\u043e\" \u0431\u0430\u0441\u043a\u044b\u0447\u044b\u043d \u0431\u0430\u0441\u0443\u0443 \u043c\u0435\u043d\u0435\u043d \u0442\u0430\u043d\u0434\u0430\u043f \u0430\u043b\u0441\u0430\u04a3\u044b\u0437 \u0431\u043e\u043b\u043e\u0442.",
    "This is the list of chosen %s. You may remove some by selecting them in the box below and then clicking the \"Remove\" arrow between the two boxes.": "\u0411\u0443\u043b \u0442\u0430\u043d\u0434\u0430\u043b\u0433\u0430\u043d %s \u0434\u0430\u0430\u043d\u0430. \u0421\u0438\u0437 \u0430\u043b\u0430\u0440\u0434\u044b\u043d \u043a\u0430\u0430\u043b\u0430\u0433\u0430\u043d\u044b\u043d \u0442\u04e9\u043c\u04e9\u043d\u0434\u04e9 \u043a\u0443\u0442\u0443\u0434\u0430\u043d \"\u04e8\u0447\u04af\u0440\" \u0431\u0430\u0441\u043a\u044b\u0447\u044b\u043d \u0431\u0430\u0441\u0443\u0443 \u043c\u0435\u043d\u0435\u043d \u04e9\u0447\u04af\u0440\u04e9 \u0430\u043b\u0430\u0441\u044b\u0437.",
    "Today": "\u0411\u04af\u0433\u04af\u043d",
    "Tomorrow": "\u042d\u0440\u0442\u0435\u04a3",
    "Type into this box to filter down the list of available %s.": "\u0416\u0435\u0442\u043a\u0438\u043b\u0438\u043a\u0442\u04af\u04af %s \u0434\u0430\u0430\u043d\u0430 \u0442\u0438\u0437\u043c\u0435\u0441\u0438\u043d \u0447\u044b\u043f\u043a\u0430\u043b\u043e\u043e \u04af\u0447\u04af\u043d \u0442\u04e9\u043c\u04e9\u043d\u043a\u04af \u043a\u0443\u0442\u0443\u0433\u0430 \u0436\u0430\u0437\u044b\u04a3\u044b\u0437.",
    "Warning: you have unsaved changes": "Advertencia: Tiene cambios que no ha guardado",
    "Yesterday": "\u041a\u0435\u0447\u044d\u044d",
    "You have selected an action, and you haven't made any changes on individual fields. You're probably looking for the Go button rather than the Save button.": "Ha seleccionado una acci\u00f3n y no ha hecho ning\u00fan cambio en campos individuales. Probablemente est\u00e9 buscando el bot\u00f3n Ejecutar en lugar del bot\u00f3n Guardar.",
    "You have selected an action, and you haven\u2019t made any changes on individual fields. You\u2019re probably looking for the Go button rather than the Save button.": "\u0421\u0438\u0437 \u0430\u0440\u0430\u043a\u0435\u0442\u0442\u0438 \u0442\u0430\u043d\u0434\u0430\u0434\u044b\u04a3\u044b\u0437 \u0436\u0430\u043d\u0430 \u04e9\u0437\u04af\u043d\u0447\u04e9 \u0430\u0439\u043c\u0430\u043a\u0442\u0430\u0440\u0434\u0430 \u04e9\u0437\u0433\u04e9\u0440\u0442\u04af\u04af \u043a\u0438\u0440\u0433\u0438\u0437\u0433\u0435\u043d \u0436\u043e\u043a\u0441\u0443\u0437. \u0421\u0438\u0437 \u0421\u0430\u043a\u0442\u043e\u043e\u043d\u0443\u043d \u043e\u0440\u0434\u0443\u043d\u0430 \u0416\u04e9\u043d\u04e9 \u0431\u0430\u0441\u043a\u044b\u0447\u044b\u043d \u0431\u0430\u0441\u0443\u0443\u04a3\u0443\u0437 \u043a\u0435\u0440\u0435\u043a.",
    "You have selected an action, but you haven't saved your changes to individual fields yet. Please click OK to save. You'll need to re-run the action.": "Ha seleccionado una acci\u00f3n, pero no ha guardado los cambios en los campos individuales todav\u00eda. Pulse OK para guardar. Tendr\u00e1 que volver a ejecutar la acci\u00f3n.",
    "You have selected an action, but you haven\u2019t saved your changes to individual fields yet. Please click OK to save. You\u2019ll need to re-run the action.": "\u0421\u0438\u0437 \u0430\u0440\u0430\u043a\u0435\u0442\u0442\u0438 \u0442\u0430\u043d\u0434\u0430\u0434\u044b\u04a3\u044b\u0437 \u0431\u0438\u0440\u043e\u043a \u04e9\u0437\u04af\u043d\u0447\u04e9 \u0430\u0439\u043c\u0430\u043a\u0442\u0430\u0440\u0434\u044b \u0441\u0430\u043a\u0442\u0430\u0439 \u044d\u043b\u0435\u043a\u0441\u0438\u0437. \u0421\u0430\u043a\u0442\u043e\u043e \u04af\u0447\u04af\u043d \u041e\u041a \u0442\u0443 \u0431\u0430\u0441\u044b\u04a3\u044b\u0437. \u0421\u0438\u0437 \u0430\u0440\u0430\u043a\u0435\u0442\u0442\u0438 \u043a\u0430\u0439\u0442\u0430\u043b\u0430\u0448\u044b\u04a3\u044b\u0437 \u043a\u0435\u0440\u0435\u043a.",
    "You have unsaved changes on individual editable fields. If you run an action, your unsaved changes will be lost.": "\u0421\u0438\u0437 \u04e9\u0437\u04af\u043d\u0447\u04e9 \u0430\u0439\u043c\u0430\u043a\u0442\u0430\u0440\u0434\u0430 \u0441\u0430\u043a\u0442\u0430\u043b\u0431\u0430\u0433\u0430\u043d \u04e9\u0437\u0433\u04e9\u0440\u0442\u04af\u04af\u043b\u04e9\u0440\u0433\u04e9 \u044d\u044d\u0441\u0438\u0437. \u042d\u0433\u0435\u0440 \u0441\u0438\u0437 \u0431\u0443\u043b \u0430\u0440\u0430\u043a\u0435\u0442\u0442\u0438 \u0436\u0430\u0441\u0430\u0441\u0430\u04a3\u044b\u0437 \u0441\u0430\u043a\u0442\u0430\u043b\u0431\u0430\u0433\u0430\u043d \u04e9\u0437\u0433\u04e9\u0440\u04af\u04af\u043b\u04e9\u0440 \u0442\u0435\u043a\u043a\u0435 \u043a\u0435\u0442\u0435\u0442.",
    "deselect all": "deseleccionar todos",
    "one letter Friday\u0004F": "\u0416\u0443\u043c\u0430",
    "one letter Monday\u0004M": "\u0414\u04af\u0439",
    "one letter Saturday\u0004S": "\u0418\u0448\u0435",
    "one letter Sunday\u0004S": "\u0416\u0435\u043a",
    "one letter Thursday\u0004T": "\u0411\u0435\u0439",
    "one letter Tuesday\u0004T": "\u0428\u0435\u0439",
    "one letter Wednesday\u0004W": "\u0428\u0430\u0440",
    "select all": "seleccionar todos"
  };
  for (var key in newcatalog) {
    django.catalog[key] = newcatalog[key];
  }
  

  if (!django.jsi18n_initialized) {
    django.gettext = function(msgid) {
      var value = django.catalog[msgid];
      if (typeof(value) == 'undefined') {
        return msgid;
      } else {
        return (typeof(value) == 'string') ? value : value[0];
      }
    };

    django.ngettext = function(singular, plural, count) {
      var value = django.catalog[singular];
      if (typeof(value) == 'undefined') {
        return (count == 1) ? singular : plural;
      } else {
        return value.constructor === Array ? value[django.pluralidx(count)] : value;
      }
    };

    django.gettext_noop = function(msgid) { return msgid; };

    django.pgettext = function(context, msgid) {
      var value = django.gettext(context + '\x04' + msgid);
      if (value.indexOf('\x04') != -1) {
        value = msgid;
      }
      return value;
    };

    django.npgettext = function(context, singular, plural, count) {
      var value = django.ngettext(context + '\x04' + singular, context + '\x04' + plural, count);
      if (value.indexOf('\x04') != -1) {
        value = django.ngettext(singular, plural, count);
      }
      return value;
    };

    django.interpolate = function(fmt, obj, named) {
      if (named) {
        return fmt.replace(/%\(\w+\)s/g, function(match){return String(obj[match.slice(2,-2)])});
      } else {
        return fmt.replace(/%s/g, function(match){return String(obj.shift())});
      }
    };


    /* formatting library */

    django.formats = {
    "DATETIME_FORMAT": "j E Y \u0436. G:i",
    "DATETIME_INPUT_FORMATS": [
      "%d.%m.%Y %H:%M:%S",
      "%d.%m.%Y %H:%M:%S.%f",
      "%d.%m.%Y %H:%M",
      "%d.%m.%Y",
      "%d.%m.%y %H:%M:%S",
      "%d.%m.%y %H:%M:%S.%f",
      "%d.%m.%y %H:%M",
      "%d.%m.%y",
      "%Y-%m-%d %H:%M:%S",
      "%Y-%m-%d %H:%M:%S.%f",
      "%Y-%m-%d %H:%M",
      "%Y-%m-%d"
    ],
    "DATE_FORMAT": "j E Y \u0436.",
    "DATE_INPUT_FORMATS": [
      "%d.%m.%Y",
      "%d.%m.%y",
      "%Y-%m-%d"
    ],
    "DECIMAL_SEPARATOR": ".",
    "FIRST_DAY_OF_WEEK": 1,
    "MONTH_DAY_FORMAT": "j F",
    "NUMBER_GROUPING": 3,
    "SHORT_DATETIME_FORMAT": "d.m.Y H:i",
    "SHORT_DATE_FORMAT": "d.m.Y",
    "THOUSAND_SEPARATOR": "\u00a0",
    "TIME_FORMAT": "G:i",
    "TIME_INPUT_FORMATS": [
      "%H:%M:%S",
      "%H:%M:%S.%f",
      "%H:%M"
    ],
    "YEAR_MONTH_FORMAT": "F Y \u0436."
  };

    django.get_format = function(format_type) {
      var value = django.formats[format_type];
      if (typeof(value) == 'undefined') {
        return format_type;
      } else {
        return value;
      }
    };

    /* add to global namespace */
    globals.pluralidx = django.pluralidx;
    globals.gettext = django.gettext;
    globals.ngettext = django.ngettext;
    globals.gettext_noop = django.gettext_noop;
    globals.pgettext = django.pgettext;
    globals.npgettext = django.npgettext;
    globals.interpolate = django.interpolate;
    globals.get_format = django.get_format;

    django.jsi18n_initialized = true;
  }

}(this));

