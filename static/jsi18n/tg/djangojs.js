

(function(globals) {

  var django = globals.django || (globals.django = {});

  
  django.pluralidx = function(n) {
    var v=(n != 1);
    if (typeof(v) == 'boolean') {
      return v ? 1 : 0;
    } else {
      return v;
    }
  };
  

  /* gettext library */

  django.catalog = django.catalog || {};
  
  var newcatalog = {
    "%(sel)s of %(cnt)s selected": [
      "%(sel)s de %(cnt)s seleccionado",
      "%(sel)s de  %(cnt)s seleccionados"
    ],
    "6 a.m.": "6-\u0438 \u0441\u0430\u04b3\u0430\u0440",
    "6 p.m.": "6-\u0438 \u0431\u0435\u0433\u043e\u04b3\u04e3",
    "Add": "A\u00f1adir",
    "April": "\u0410\u043f\u0440\u0435\u043b",
    "August": "\u0410\u0432\u0433\u0443\u0441\u0442",
    "Available %s": "\u0414\u0430\u0441\u0442\u0440\u0430\u0441 %s",
    "Cancel": "\u0418\u043d\u043a\u043e\u0440",
    "Choose": "\u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043a\u0430\u0440\u0434\u0430\u043d",
    "Choose a Date": "\u0421\u0430\u043d\u0430\u0440\u043e \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043a\u0443\u043d\u0435\u0434",
    "Choose a Time": "\u0412\u0430\u049b\u0442\u0440\u043e \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043a\u0443\u043d\u0435\u0434",
    "Choose a time": "\u0412\u0430\u049b\u0442\u0440\u043e \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043a\u0443\u043d\u0435\u0434",
    "Choose all": "\u0418\u043d\u0442\u0438\u0445\u043e\u0431\u0438 \u043a\u0443\u043b\u043b",
    "Chosen %s": "%s -\u04b3\u043e\u0438 \u0438\u043d\u0442\u0438\u0445\u043e\u0431\u0448\u0443\u0434\u0430",
    "Click to choose all %s at once.": "\u0411\u0430\u0440\u043e\u0438 \u044f\u043a\u0431\u043e\u0440\u0430 \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043d\u0430\u043c\u0443\u0434\u0430\u043d\u0438 \u043a\u0443\u043b\u043b\u0438 %s \u0438\u043d\u04b7\u043e\u0440\u043e \u043f\u0430\u0445\u0448 \u043d\u0430\u043c\u043e\u0435\u0434.",
    "Click to remove all chosen %s at once.": "\u041f\u0430\u0445\u0448 \u043a\u0443\u043d\u0435\u0434 \u0431\u0430\u0440\u043e\u0438 \u044f\u043a\u0431\u043e\u0440\u0430 \u043d\u0435\u0441\u0442 \u043a\u0430\u0440\u0434\u0430\u043d\u0438 \u04b3\u0430\u043c\u0430\u0438 %s.",
    "December": "\u0414\u0435\u043a\u0430\u0431\u0440",
    "Delete": "Eliminar",
    "February": "\u0424\u0435\u0432\u0440\u0430\u043b",
    "Filter": "\u041f\u043e\u043b\u043e",
    "General": "General",
    "Hide": "\u041f\u0438\u043d\u04b3\u043e\u043d \u043a\u0430\u0440\u0434\u0430\u043d",
    "January": "\u042f\u043d\u0432\u0430\u0440",
    "July": "\u0418\u044e\u043b",
    "June": "\u0418\u044e\u043d",
    "March": "\u041c\u0430\u0440\u0442",
    "May": "\u041c\u0430\u0439",
    "Midnight": "\u041d\u0438\u0441\u0444\u0438\u0448\u0430\u0431\u04e3",
    "Noon": "\u041d\u0438\u0441\u0444\u0438\u0440\u04ef\u0437\u04e3",
    "Note: You are %s hour ahead of server time.": [
      "Nota: Usted esta a %s horas por delante de la hora del servidor.",
      "Nota: Usted va %s horas por delante de la hora del servidor."
    ],
    "Note: You are %s hour behind server time.": [
      "Nota: Usted esta a %s hora de retraso de tiempo de servidor.",
      "Nota: Usted va %s horas por detr\u00e1s de la hora del servidor."
    ],
    "November": "\u041d\u043e\u044f\u0431\u0440",
    "Now": "\u04b2\u043e\u0437\u0438\u0440",
    "October": "\u041e\u043a\u0442\u044f\u0431\u0440",
    "Remove": "\u041d\u0435\u0441\u0442 \u043a\u0430\u0440\u0434\u0430\u043d",
    "Remove all": "\u041d\u0435\u0441\u0442 \u043a\u0430\u0440\u0434\u0430\u043d \u0431\u0430 \u0442\u0430\u0432\u0440\u0438 \u043a\u0443\u043b\u043b",
    "September": "\u0421\u0435\u043d\u0442\u044f\u0431\u0440",
    "Show": "\u041d\u0438\u0448\u043e\u043d \u0434\u043e\u0434\u0430\u043d",
    "This is the list of available %s. You may choose some by selecting them in the box below and then clicking the \"Choose\" arrow between the two boxes.": "\u0418\u043d \u0440\u0443\u0439\u0445\u0430\u0442\u0438 %s - \u04b3\u043e\u0438 \u0434\u0430\u0441\u0442\u0440\u0430\u0441. \u0428\u0443\u043c\u043e \u043c\u0435\u0442\u0430\u0432\u043e\u043d\u0435\u0434 \u044f\u043a\u0447\u0430\u043d\u0434\u0442\u043e \u0430\u0437 \u0438\u043d\u04b3\u043e\u0440\u043e \u0434\u0430\u0440 \u043c\u0430\u0439\u0434\u043e\u043d\u0438\u043f\u043e\u0451\u043d \u0431\u043e \u043f\u0430\u0445\u0448\u0438 \u0442\u0443\u0433\u043c\u0430\u0438 \\'\u0418\u043d\u0442\u0438\u0445\u043e\u0431 \u043a\u0430\u0440\u0434\u0430\u043d'\\ \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043d\u0430\u043c\u043e\u0435\u0434.",
    "This is the list of chosen %s. You may remove some by selecting them in the box below and then clicking the \"Remove\" arrow between the two boxes.": "\u0418\u043d \u0440\u0443\u0439\u0445\u0430\u0442\u0438 %s - \u04b3\u043e\u0438 \u0438\u043d\u0442\u0438\u0445\u043e\u0431\u0448\u0443\u0434\u0430. \u0428\u0443\u043c\u043e \u043c\u0435\u0442\u0430\u0432\u043e\u043d\u0435\u0434 \u044f\u043a\u0447\u0430\u043d\u0434\u0442\u043e \u0430\u0437 \u0438\u043d\u04b3\u043e\u0440\u043e \u0434\u0430\u0440 \u043c\u0430\u0439\u0434\u043e\u043d\u0438\u043f\u043e\u0451\u043d \u0431\u043e \u043f\u0430\u0445\u0448\u0438 \u0442\u0443\u0433\u043c\u0430\u0438 \\'\u041d\u0435\u0441\u0442 \u043a\u0430\u0440\u0434\u0430\u043d'\\ \u043d\u0435\u0441\u0442 \u0441\u043e\u0437\u0435\u0434.",
    "Today": "\u0418\u043c\u0440\u04ef\u0437",
    "Tomorrow": "\u0424\u0430\u0440\u0434\u043e",
    "Type into this box to filter down the list of available %s.": "\u0411\u0430\u0440\u043e\u0438 \u0431\u0430\u0440\u043e\u0432\u0430\u0440\u0434\u0430\u043d\u0438 \u0440\u04ef\u0439\u0445\u0430\u0442\u0438 %s. -\u04b3\u043e\u0438 \u0434\u0430\u0441\u0442\u0440\u0430\u0441, \u0431\u0430 \u0432\u043e\u0440\u0438\u0434\u0441\u043e\u0437\u0438\u0438 \u043c\u0430\u0442\u043d\u0438 \u043b\u043e\u0437\u0438\u043c\u0430 \u0448\u0443\u0440\u04ef\u044a \u043a\u0443\u043d\u0435\u0434",
    "Warning: you have unsaved changes": "Advertencia: Tiene cambios que no ha guardado",
    "Yesterday": "\u0414\u0438\u0440\u04ef\u0437",
    "You have selected an action, and you haven't made any changes on individual fields. You're probably looking for the Go button rather than the Save button.": "Ha seleccionado una acci\u00f3n y no ha hecho ning\u00fan cambio en campos individuales. Probablemente est\u00e9 buscando el bot\u00f3n Ejecutar en lugar del bot\u00f3n Guardar.",
    "You have selected an action, and you haven\u2019t made any changes on individual fields. You\u2019re probably looking for the Go button rather than the Save button.": "\"\u0428\u0443\u043c\u043e \u0430\u043c\u0430\u043b\u0440\u043e\u0440 \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043d\u0430\u043c\u0443\u0434\u0435\u0434, \u0432\u0430\u043b\u0435 \u0442\u0430\u0493\u0439\u0438\u0440\u043e\u0442 \u0432\u043e\u0440\u0438\u0434 \u043d\u0430\u0441\u043e\u0445\u0442\u0435\u0434.\"\n\"\u042d\u04b3\u0442\u0438\u043c\u043e\u043b \u0448\u0443\u043c\u043e \u043c\u0435\u0445\u043e\u0441\u0442\u0435\u0434 \u0431\u0430 \u04b7\u043e\u0438 \u0442\u0443\u0433\u043c\u0430\u0438 \\'\u04b2\u0438\u0444\u0437 \u043a\u0430\u0440\u0434\u0430\u043d'\\, \u0430\u0437 \u0442\u0443\u0433\u043c\u0430\u0438 \\'\u0418\u04b7\u0440\u043e \u043a\u0430\u0440\u0434\u0430\u043d'\\ \u0438\u0441\u0442\u0438\u0444\u043e\u0434\u0430 \u043d\u0430\u043c\u043e\u0435\u0434.\"\n\"\u0410\u0433\u0430\u0440 \u0447\u0443\u043d\u0438\u043d \u0431\u043e\u0448\u0430\u0434, \u043e\u043d \u0433\u043e\u04b3 \u0442\u0443\u0433\u043c\u0430\u0438 \\'\u0418\u043d\u043a\u043e\u0440'\\ \u043f\u0430\u0445\u0448 \u043a\u0443\u043d\u0435\u0434, \u0442\u043e \u043a\u0438 \u0431\u0430 \u043c\u0430\u0439\u0434\u043e\u043d\u0438 \u0442\u0430\u04b3\u0440\u0438\u0440\u043a\u0443\u043d\u04e3 \u0431\u0430\u0440\u0433\u0430\u0440\u0434\u0435\u0434.\"",
    "You have selected an action, but you haven't saved your changes to individual fields yet. Please click OK to save. You'll need to re-run the action.": "Ha seleccionado una acci\u00f3n, pero no ha guardado los cambios en los campos individuales todav\u00eda. Pulse OK para guardar. Tendr\u00e1 que volver a ejecutar la acci\u00f3n.",
    "You have selected an action, but you haven\u2019t saved your changes to individual fields yet. Please click OK to save. You\u2019ll need to re-run the action.": "\u0428\u0443\u043c\u043e \u0430\u043c\u0430\u043b\u0440\u043e \u0438\u043d\u0442\u0438\u0445\u043e\u0431 \u043d\u0430\u043c\u0443\u0434\u0435\u0434, \u0432\u0430\u043b\u0435 \u04b3\u0430\u043d\u0443\u0437 \u0442\u0430\u0493\u0439\u0438\u0440\u043e\u0442\u04b3\u043e\u0438 \u0432\u043e\u0440\u0438\u0434 \u043a\u0430\u0440\u0434\u0430\u0448\u0443\u0434\u0430 \u04b3\u0438\u0444\u0437 \u043d\u0430\u0448\u0443\u0434\u0430\u0430\u043d\u0434.\"\n\"\u0411\u0430\u0440\u043e\u0438 \u04b3\u0438\u0444\u0437 \u043d\u0430\u043c\u0443\u0434\u0430\u043d\u0438 \u043e\u043d\u04b3\u043e \u0431\u0430 \u0442\u0443\u0433\u043c\u0430\u0438 '\u041e\u041a' \u043f\u0430\u0445\u0448 \u043d\u0430\u043c\u043e\u0435\u0434.\"\n\"\u0421\u0438\u043f\u0430\u0441 \u0448\u0443\u043c\u043e\u0440\u043e \u043b\u043e\u0437\u0438\u043c \u043c\u0435\u043e\u044f\u0434, \u043a\u0438 \u0430\u043c\u0430\u043b\u0440\u043e \u0442\u0430\u043a\u0440\u043e\u0440\u0430\u043d \u0438\u04b7\u0440\u043e \u043d\u0430\u043c\u043e\u0435\u0434",
    "You have unsaved changes on individual editable fields. If you run an action, your unsaved changes will be lost.": "\u0422\u0430\u0493\u0439\u0438\u0440\u043e\u0442\u04b3\u043e\u0438 \u04b3\u0438\u0444\u0437\u043d\u0430\u043a\u0430\u0440\u0434\u0430\u0448\u0443\u0434\u0430 \u0434\u0430\u0440 \u043c\u0430\u0439\u0434\u043e\u043d\u0438 \u0442\u0430\u04b3\u0440\u0438\u0440 \u043c\u0430\u0432\u04b7\u0443\u0434\u0430\u043d\u0434. \u0410\u0433\u0430\u0440\u0448\u0443\u043c\u043e \u0438\u04b7\u0440\u043e\u0438 \u0430\u043c\u0430\u043b\u0440\u043e \u0434\u0430\u0432\u043e\u043c \u0434\u0438\u04b3\u0435\u0434, \u043e\u043d\u04b3\u043e \u043d\u0435\u0441\u0442 \u0445\u043e\u04b3\u0430\u043d\u0434 \u0448\u0443\u0434.",
    "deselect all": "deseleccionar todos",
    "one letter Friday\u0004F": "\u04b6",
    "one letter Monday\u0004M": "\u0414",
    "one letter Saturday\u0004S": "\u0428",
    "one letter Sunday\u0004S": "\u042f",
    "one letter Thursday\u0004T": "\u041f",
    "one letter Tuesday\u0004T": "\u0421",
    "one letter Wednesday\u0004W": "\u0427",
    "select all": "seleccionar todos"
  };
  for (var key in newcatalog) {
    django.catalog[key] = newcatalog[key];
  }
  

  if (!django.jsi18n_initialized) {
    django.gettext = function(msgid) {
      var value = django.catalog[msgid];
      if (typeof(value) == 'undefined') {
        return msgid;
      } else {
        return (typeof(value) == 'string') ? value : value[0];
      }
    };

    django.ngettext = function(singular, plural, count) {
      var value = django.catalog[singular];
      if (typeof(value) == 'undefined') {
        return (count == 1) ? singular : plural;
      } else {
        return value.constructor === Array ? value[django.pluralidx(count)] : value;
      }
    };

    django.gettext_noop = function(msgid) { return msgid; };

    django.pgettext = function(context, msgid) {
      var value = django.gettext(context + '\x04' + msgid);
      if (value.indexOf('\x04') != -1) {
        value = msgid;
      }
      return value;
    };

    django.npgettext = function(context, singular, plural, count) {
      var value = django.ngettext(context + '\x04' + singular, context + '\x04' + plural, count);
      if (value.indexOf('\x04') != -1) {
        value = django.ngettext(singular, plural, count);
      }
      return value;
    };

    django.interpolate = function(fmt, obj, named) {
      if (named) {
        return fmt.replace(/%\(\w+\)s/g, function(match){return String(obj[match.slice(2,-2)])});
      } else {
        return fmt.replace(/%s/g, function(match){return String(obj.shift())});
      }
    };


    /* formatting library */

    django.formats = {
    "DATETIME_FORMAT": "j E Y \u0433. G:i",
    "DATETIME_INPUT_FORMATS": [
      "%d.%m.%Y %H:%M:%S",
      "%d.%m.%Y %H:%M:%S.%f",
      "%d.%m.%Y %H:%M",
      "%d.%m.%Y",
      "%d.%m.%y %H:%M:%S",
      "%d.%m.%y %H:%M:%S.%f",
      "%d.%m.%y %H:%M",
      "%d.%m.%y",
      "%Y-%m-%d %H:%M:%S",
      "%Y-%m-%d %H:%M:%S.%f",
      "%Y-%m-%d %H:%M",
      "%Y-%m-%d"
    ],
    "DATE_FORMAT": "j E Y \u0433.",
    "DATE_INPUT_FORMATS": [
      "%d.%m.%Y",
      "%d.%m.%y",
      "%Y-%m-%d"
    ],
    "DECIMAL_SEPARATOR": ",",
    "FIRST_DAY_OF_WEEK": 1,
    "MONTH_DAY_FORMAT": "j F",
    "NUMBER_GROUPING": 3,
    "SHORT_DATETIME_FORMAT": "d.m.Y H:i",
    "SHORT_DATE_FORMAT": "d.m.Y",
    "THOUSAND_SEPARATOR": "\u00a0",
    "TIME_FORMAT": "G:i",
    "TIME_INPUT_FORMATS": [
      "%H:%M:%S",
      "%H:%M:%S.%f",
      "%H:%M"
    ],
    "YEAR_MONTH_FORMAT": "F Y \u0433."
  };

    django.get_format = function(format_type) {
      var value = django.formats[format_type];
      if (typeof(value) == 'undefined') {
        return format_type;
      } else {
        return value;
      }
    };

    /* add to global namespace */
    globals.pluralidx = django.pluralidx;
    globals.gettext = django.gettext;
    globals.ngettext = django.ngettext;
    globals.gettext_noop = django.gettext_noop;
    globals.pgettext = django.pgettext;
    globals.npgettext = django.npgettext;
    globals.interpolate = django.interpolate;
    globals.get_format = django.get_format;

    django.jsi18n_initialized = true;
  }

}(this));

