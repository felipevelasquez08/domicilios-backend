#!/bin/bash

echo Push migrations to database
./manage.py migrate

echo Starting Gunicorn
./manage.py runserver 0.0.0.0:8000
