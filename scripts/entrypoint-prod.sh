#!/bin/bash

echo Build migrations files
python manage.py makemigrations --settings=flipos_express.settings.prod

echo Push migrations to database
python manage.py migrate --settings=flipos_express.settings.prod

echo Push collectstatic
python manage.py collectstatic --noinput --settings=flipos_express.settings.prod

echo Starting Gunicorn
exec gunicorn flipos_express.wsgi:application \
    --env DJANGO_SETTINGS_MODULE=flipos_express.settings.prod \
    --name FliposExpress \
    --bind 0.0.0.0:"$PORT" \
    --workers 3 \
    --timeout 3600 \
"$@"