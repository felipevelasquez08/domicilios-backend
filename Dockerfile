FROM python:3.8.6-slim

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update
RUN apt-get install -y libpq-dev python3-dev build-essential gettext python-mysqldb binutils libproj-dev gdal-bin python-pil default-libmysqlclient-dev

ENV APP_PATH=/src

RUN mkdir $APP_PATH
WORKDIR $APP_PATH

COPY requirements.txt $APP_PATH/
RUN pip install -r requirements.txt
COPY . $APP_PATH/

RUN chmod -R 755 scripts/
ENTRYPOINT ./scripts/entrypoint-${ENVIRONMENT}.sh