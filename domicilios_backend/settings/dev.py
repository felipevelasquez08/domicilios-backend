from os import environ

from .base import *

ALLOWED_HOSTS = ALLOWED_HOSTS + ['localhost', ]

MYSQL_USER = environ.get('MYSQL_USER', 'flipos_user')
MYSQL_PASSWORD = environ.get('MYSQL_PASSWORD', 'flipos.express2020')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'database',
        'PORT': 5432,
    }
}
