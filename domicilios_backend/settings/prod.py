# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

import dj_database_url

from .base import *

ALLOWED_HOSTS = ['192.168.0.14', 'flipos-express.herokuapp.com', ]

# DATABASES = {
#     'default': {
#         'ENGINE': environ.get('MYSQL_ENGINE', ENGINE),
#         'NAME': environ.get('MYSQL_DATABASE', 'root'),
#         'USER': environ.get('MYSQL_USER', 'root'),
#         'PASSWORD': environ.get('MYSQL_PASSWORD', ''),
#         'HOST': environ.get('MYSQL_HOST', 'database'),
#         'PORT': environ.get('MYSQL_PORT', 3306),
#         'OPTIONS': {
#             'charset': 'utf8',
#         }
#     }
# }

DATABASES = {'default': dj_database_url.config(conn_max_age=600, ssl_require=True)}

