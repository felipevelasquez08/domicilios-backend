from crum import get_current_user
from django.db import models

from core.constants import ACTIVE, CREATION_DATE, CREATED_BY, UPDATE_DATE, UPDATE_BY, RELATED_NAME


class Audit(models.Model):
    active = models.BooleanField(verbose_name=ACTIVE, default=True)
    creation_date = models.DateTimeField(verbose_name=CREATION_DATE, auto_now_add=True)
    created_by = models.ForeignKey('users.User', verbose_name=CREATED_BY, on_delete=models.CASCADE,
                                   related_name=f'{RELATED_NAME}created_by', null=True, blank=True)
    update_date = models.DateTimeField(verbose_name=UPDATE_DATE, auto_now=True, null=True, blank=True)
    update_by = models.ForeignKey('users.User', verbose_name=UPDATE_BY, on_delete=models.CASCADE,
                                  related_name=f'{RELATED_NAME}update_by', null=True, blank=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        user = get_current_user()
        if not self.id:
            self.created_by = user
        else:
            self.update_by = user
        super(__class__, self).save(*args, **kwargs)
