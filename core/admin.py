from django.contrib import admin
from django.utils.safestring import mark_safe

from core.constants import EMPTY


def _build_preview(pk, image):
    if pk and image:
        return mark_safe(
            '<img src="{}" style="-ms-interpolation-mode: bicubic;max-width:200px;max-height:auto;"/>'.format(
                image.url))
    return "Selecciona una imagen y guardala para poder ver la vista previa."


class AuditAdmin(admin.ModelAdmin):
    fieldsets = [('Auditoria', {
        'fields': (('created_by', 'creation_date'), ('update_by', 'update_date'), 'active'),
    }), ]
    readonly_fields = ('created_by', 'creation_date', 'update_by', 'update_date')
    empty_value_display = EMPTY
