# Audit

CREATION_DATE = 'Fecha de creación'
CREATED_BY = 'Creado por'
UPDATE_DATE = 'Fecha de actualización'
UPDATE_BY = 'Actualizado por'
ACTIVE = '¿Está activo?'
RELATED_NAME = '%(class)s_'

EMPTY = '(Vacio)'
TOKEN = 'Token'
NAME = 'Nombre'
ICON = 'Icono'
PREVIEW = 'Vista previa'
