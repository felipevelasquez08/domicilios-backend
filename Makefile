install:
	docker-compose exec backend pip install -r requirements.txt

makemigrations:
	docker-compose exec backend ./manage.py makemigrations

migrate:
	docker-compose exec backend ./manage.py migrate

collectstatic:
	docker-compose exec backend ./manage.py collectstatic --noinput

createsuperuser:
	docker-compose exec backend ./manage.py createsuperuser

dumpdata:
	docker-compose exec backend ./manage.py dumpdata > domicilios.json

loaddata:
	docker-compose exec backend ./manage.py loaddata -e contenttypes -e authtoken -e admin -e auth.permission domicilios.json

flush:
	docker-compose exec backend ./manage.py flush

test:
	docker-compose exec backend ./manage.py test

heroku:
	heroku container:push web -a flipos-express
	heroku container:release web -a flipos-express

up:
	docker-compose -f docker-compose.yml up -d --build

logs:
	docker-compose -f docker-compose.yml logs -f

logs_database:
	docker-compose -f docker-compose.yml logs -f database

logs_backend:
	docker-compose -f docker-compose.yml logs -f backend

logs_nginx:
	docker-compose -f docker-compose.yml logs -f nginx

up_database:
	docker-compose -f docker-compose.yml up -d --build database

up_alone:
	docker-compose -f docker-compose.yml up -d

down:
	docker-compose -f docker-compose.yml down

down_all:
	docker-compose -f docker-compose.yml down -v