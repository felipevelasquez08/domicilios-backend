# User

PROFILE_PICTURE = 'Foto de perfil'
FIRST_NAME = 'Nombres'
LAST_NAME = 'Apellidos'
PHONE = 'Número telefónico'
VEHICLE_PLATE = 'Placa del vehículo'
VEHICLE_BRAND = 'Marca del vehículo'
IDENTIFICATION_NUMBER = 'Número de identificación'
FRONT_PHOTO_IDENTIFICATION = 'Foto delantera de la identificación'
BACK_PHOTO_IDENTIFICATION = 'Foto trasera de la identificación'
FULL_NAME = 'Nombre completo'
GENDER = 'Género'
GENDER_CHOICES = (
    (1, 'Hombre'),
    (2, 'Mujer')
)
PASSWORD = 'Contraseña'
NEW_PASSWORD = 'Nueva contraseña'
RE_PASSWORD = 'Repetir contraseña'
GROUP = 'Grupo'
DATE_JOINED = "Fecha de alta"
COUNT_PERMISSIONS = 'Cantidad de permisos'

# Administrator

ADMINISTRATOR = 'Administrador'
ADMINISTRATORS = 'Administradores'

# SuperAdmin
SUPERADMIN = 'Super administrador'
SUPERADMINS = 'Super administradores'
IS_ADMIN = '¿Es administrador?'
