from django import forms

PASSWORD_LEN = 6


def password_is_empty():
    return forms.ValidationError('Este campo no puede estar vacío.')


def password_not_valid():
    return forms.ValidationError(f'La contraseña debe tener minimo {PASSWORD_LEN} o más caracteres.')


def password_not_match():
    return forms.ValidationError('Las contraseñas no coinciden.')
