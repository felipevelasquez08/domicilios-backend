from django.contrib.auth.models import AbstractUser
from django.db import models
# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from apps.users.utilities.constants import PROFILE_PICTURE, FIRST_NAME, LAST_NAME, PHONE, VEHICLE_PLATE, VEHICLE_BRAND, \
    IDENTIFICATION_NUMBER, FRONT_PHOTO_IDENTIFICATION, BACK_PHOTO_IDENTIFICATION, GENDER, GENDER_CHOICES, DATE_JOINED, \
    SUPERADMIN, SUPERADMINS, IS_ADMIN, ADMINISTRATOR, ADMINISTRATORS
from apps.vehicles.models import Model
from core.constants import TOKEN
from core.models import Audit


@receiver(post_save, sender='users.User')
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        token = Token.objects.create(user=instance)
        user_id = instance.id
        User.objects.filter(id=user_id).update(token=token.key)


class User(AbstractUser, Audit):
    profile_picture = models.ImageField(verbose_name=PROFILE_PICTURE, upload_to='user/profile')
    first_name = models.CharField(verbose_name=FIRST_NAME, max_length=150, )
    last_name = models.CharField(verbose_name=LAST_NAME, max_length=150, )
    phone = models.CharField(verbose_name=PHONE, max_length=10, )
    vehicle_plate = models.CharField(verbose_name=VEHICLE_PLATE, max_length=15, null=True, blank=True)
    vehicle_brand = models.ForeignKey(Model, verbose_name=VEHICLE_BRAND, max_length=50, on_delete=models.CASCADE,
                                      null=True, blank=True)
    identification_number = models.IntegerField(verbose_name=IDENTIFICATION_NUMBER, )
    gender = models.SmallIntegerField(verbose_name=GENDER, default=1, choices=GENDER_CHOICES)
    front_photo_identification = models.ImageField(verbose_name=FRONT_PHOTO_IDENTIFICATION, null=True, blank=True)
    back_photo_identification = models.ImageField(verbose_name=BACK_PHOTO_IDENTIFICATION, null=True, blank=True)
    token = models.CharField(verbose_name=TOKEN, max_length=150, null=True, blank=True)
    date_joined = models.DateTimeField(verbose_name=DATE_JOINED, auto_now_add=True)
    is_admin = models.BooleanField(verbose_name=IS_ADMIN, default=False)

    REQUIRED_FIELDS = ['email', 'password', 'identification_number']

    class Meta(AbstractUser.Meta):
        unique_together = [['phone'], ['identification_number']]

    def __str__(self):
        return f'{self.get_full_name()}'


class Administrator(User):
    class Meta:
        verbose_name = ADMINISTRATOR
        verbose_name_plural = ADMINISTRATORS
        proxy = True


class SuperAdmin(User):
    class Meta:
        verbose_name = SUPERADMIN
        verbose_name_plural = SUPERADMINS
        proxy = True
