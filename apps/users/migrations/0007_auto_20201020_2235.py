# Generated by Django 3.1.2 on 2020-10-21 03:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20201020_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='profile_picture',
            field=models.ImageField(default='/media/user/profile_picture_empty.png', upload_to='user/profile', verbose_name='Profile picture'),
        ),
        migrations.AlterField(
            model_name='user',
            name='token',
            field=models.CharField(max_length=150, verbose_name='Token'),
        ),
    ]
