# Generated by Django 3.1.2 on 2020-10-21 03:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_auto_20201020_2235'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='profile_picture',
            field=models.ImageField(upload_to='user/profile', verbose_name='Profile picture'),
        ),
    ]
