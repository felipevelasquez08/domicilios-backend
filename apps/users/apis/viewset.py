from rest_framework import viewsets, response, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import action
from rest_framework.views import APIView

from apps.users.apis.serializers import TokenDetailSerializer, TokenSingleSerializer, \
    UserDetailSerializer
from apps.users.models import User


class AuthLogInViewSet(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        serializer_token = TokenSingleSerializer(token, context={'request': request})
        return response.Response(serializer_token.data)


class AuthLogOutViewSet(APIView):
    def get(self, request):
        request.user.auth_token.delete()
        return response.Response(data={'detail': '¿Qué te disgustó?, Bueno… Esperamos vuelvas pronto.'},
                                 status=status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    _detail = {'deatil': 'Lo sentimos, solo puedes ver información de tu perfil.'}
    _is_authenticated = {'deatil': 'No recibimos ninguna credencial. Lo sentimos :('}

    def list(self, request, *args, **kwargs):
        try:
            token = request.user.auth_token
            data = Token.objects.filter(key=token).first()
            serializer = TokenDetailSerializer(data, context={'request': request})
            return response.Response(serializer.data)
        except User.DoesNotExist:
            return response.Response(self._is_authenticated)

    def create(self, request, *args, **kwargs):
        return response.Response(self._detail)

    @action(methods=['GET'], detail=False, url_path='is_login')
    def is_login(self, request):
        token = request.user.auth_token
        data = Token.objects.filter(key=token).first()
        serializer = TokenSingleSerializer(data, context={"request": request})
        return response.Response(serializer.data)
