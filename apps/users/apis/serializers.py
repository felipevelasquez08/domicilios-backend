from rest_framework import serializers
from rest_framework.authtoken.models import Token

from apps.users.models import User


class UserSerializer(serializers.Serializer):
    pass


class UserDetailSerializer(serializers.ModelSerializer):
    vehicle_brand = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['profile_picture', 'username', 'first_name', 'last_name', 'phone', 'email',
                  'vehicle_plate', 'vehicle_brand']

    def get_vehicle_brand(self, user):
        request = self.context.get('request')
        icon_url = user.vehicle_brand.brand.icon.url
        return request.build_absolute_uri(icon_url)


class TokenSingleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ['key', 'user']


class TokenDetailSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer()

    class Meta:
        model = Token
        fields = ['key', 'user']
