from django.contrib import admin
# Register your models here.
from django.contrib.admin import ModelAdmin
from django.contrib.auth.models import Group
from django.db.models import Count

from apps.users.forms import UserEditForm, UserAddForm, GroupForm, AdministratorAddForm, AdministratorEditForm, \
    SuperAdminAddForm, SuperAdminEditForm
from apps.users.models import SuperAdmin, User, Administrator
from apps.users.utilities.constants import FULL_NAME, COUNT_PERMISSIONS
from core.admin import _build_preview, AuditAdmin
from core.constants import EMPTY, PREVIEW

FIELDSETS = [
    ('Datos del perfil', {'fields': (
        ('profile_picture', '_profile_picture_preview'), 'username', ('password', 're_password'),
        ('token', 'date_joined'),)
    }),
    ('Datos básicos', {'fields': (
        ('first_name', 'last_name'), ('identification_number', 'gender'),
        ('front_photo_identification', '_front_photo_identification_preview'),
        ('back_photo_identification', '_back_photo_identification_preview'),
    )}),
    ('Datos de contacto', {'fields': ('phone', 'email',)}),
]

FIELDSETS_VEHICLE_DATA = [
    ('Datos del vehiculo', {'fields': ('vehicle_plate', 'vehicle_brand',)}),
]

FIELDSETS_PERMISSIONS = [
    ('Permisos', {'fields': ('groups',)}),
]


class UserBaseAdmin(AuditAdmin):
    list_display = ('_profile_picture_preview', '_full_name', 'identification_number', 'phone',)
    list_display_links = list_display
    search_fields = ('first_name',)
    empty_value_display = EMPTY
    readonly_fields = ('token', 'date_joined', '_profile_picture_preview', '_front_photo_identification_preview',
                       '_back_photo_identification_preview') + AuditAdmin.readonly_fields
    form = UserEditForm

    def _profile_picture_preview(self, obj=None):
        return _build_preview(obj.pk, obj.profile_picture)

    _profile_picture_preview.short_description = PREVIEW
    _profile_picture_preview.admin_order_field = 'username'

    def _full_name(self, obj):
        return obj.get_full_name()

    _full_name.short_description = FULL_NAME
    _full_name.admin_order_field = 'first_name'

    def _front_photo_identification_preview(self, obj=None):
        return _build_preview(obj.pk, obj.front_photo_identification)

    _front_photo_identification_preview.short_description = PREVIEW

    def _back_photo_identification_preview(self, obj=None):
        return _build_preview(obj.pk, obj.back_photo_identification)

    _back_photo_identification_preview.short_description = PREVIEW

    def get_form(self, request, obj=None, change=False, **kwargs):
        return UserAddForm if obj is None else super(__class__, self).get_form(request, obj, change, **kwargs)

    def get_fieldsets(self, request, obj=None):
        return FIELDSETS + FIELDSETS_VEHICLE_DATA + self.fieldsets


@admin.register(User)
class UserAdmin(UserBaseAdmin):

    def get_list_display(self, request):
        return super(UserAdmin, self).get_list_display(request) + ('vehicle_plate', 'vehicle_brand')

    def get_list_display_links(self, request, list_display):
        return list_display

    def get_fieldsets(self, request, obj=None):
        return FIELDSETS + FIELDSETS_VEHICLE_DATA + self.fieldsets

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(is_superuser=False, is_admin=False)


@admin.register(Administrator)
class AdministratorAdmin(UserBaseAdmin):
    form = AdministratorEditForm

    def get_fieldsets(self, request, obj=None):
        return FIELDSETS + FIELDSETS_PERMISSIONS + self.fieldsets

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(is_admin=True)

    def save_model(self, request, obj, form, change):
        obj.is_admin = True
        obj.is_staff = True
        super(__class__, self).save_model(request, obj, form, change)

    def get_form(self, request, obj=None, change=False, **kwargs):
        return AdministratorAddForm if obj is None else super(__class__, self).get_form(request, obj, change, **kwargs)


@admin.register(SuperAdmin)
class SuperAdminAdmin(UserBaseAdmin):
    form = SuperAdminEditForm

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(is_superuser=True)

    def save_model(self, request, obj, form, change):
        obj.is_superuser = True
        obj.is_staff = True
        obj.is_active = True
        super(__class__, self).save_model(request, obj, form, change)

    def get_form(self, request, obj=None, change=False, **kwargs):
        return SuperAdminAddForm if obj is None else super(__class__, self).get_form(request, obj, change, **kwargs)


admin.site.unregister(Group)


@admin.register(Group)
class GroupAdmin(ModelAdmin):
    list_display = ('name', '_count_permissions')
    form = GroupForm

    def _count_permissions(self, obj):
        return obj.count_permissions

    _count_permissions.short_description = COUNT_PERMISSIONS
    _count_permissions.admin_order_field = 'count_permissions'

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.annotate(count_permissions=Count('permissions'))
