from django import forms
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import Group

from apps.users.models import User, Administrator, SuperAdmin
from apps.users.utilities.constants import VEHICLE_BRAND, PASSWORD, RE_PASSWORD, NEW_PASSWORD, VEHICLE_PLATE
from apps.users.utilities.exceptions import password_not_valid, password_not_match, password_is_empty
from apps.vehicles.models import Model

PASSWORD_LEN = 6


def validate_password(password, cipher=True):
    if len(password) < PASSWORD_LEN:
        raise password_not_valid()
    if cipher:
        return make_password(password)
    else:
        return password


def clean_password(obj):
    password = obj.cleaned_data.get('password')
    if password:
        password = password.strip()
        return validate_password(password)


def clean_re_password(obj):
    password = obj.cleaned_data.get('password')
    if password:
        re_password = obj.cleaned_data.get('re_password')
        if re_password:
            re_password = validate_password(re_password, False)
            if check_password(re_password, password):
                return re_password
            else:
                raise password_not_match()
        else:
            raise password_is_empty()


class UserBaseForm(forms.ModelForm):
    vehicle_plate = forms.CharField(label=VEHICLE_PLATE, required=False)
    vehicle_brand = forms.ModelChoiceField(label=VEHICLE_BRAND, queryset=Model.objects.all(), required=False)

    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'identification_number': forms.NumberInput(),
        }

    def clean_password(self):
        return clean_password(self)

    def clean_re_password(self):
        return clean_re_password(self)


class UserBaseAddForm(UserBaseForm):
    password = forms.CharField(label=PASSWORD, widget=forms.PasswordInput(), strip=False)
    re_password = forms.CharField(label=RE_PASSWORD, widget=forms.PasswordInput(), strip=False)


class UserBaseEditForm(UserBaseForm):
    password = forms.CharField(label=NEW_PASSWORD, widget=forms.PasswordInput(), strip=False, required=False)
    re_password = forms.CharField(label=RE_PASSWORD, widget=forms.PasswordInput(), strip=False, required=False)

    def clean(self):
        clean_data = super(__class__, self).clean()
        new_password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if new_password is None and re_password is None:
            clean_data['password'] = self.instance.password


class UserAddForm(UserBaseAddForm):
    vehicle_plate = forms.CharField(label=VEHICLE_PLATE)
    vehicle_brand = forms.ModelChoiceField(label=VEHICLE_BRAND, queryset=Model.objects.all())


class UserEditForm(UserBaseEditForm):
    vehicle_plate = forms.CharField(label=VEHICLE_PLATE)
    vehicle_brand = forms.ModelChoiceField(label=VEHICLE_BRAND, queryset=Model.objects.all())


class AdministratorAddForm(UserBaseAddForm):
    class Meta(UserBaseForm.Meta):
        model = Administrator


class AdministratorEditForm(UserBaseEditForm):
    class Meta(UserBaseForm.Meta):
        model = Administrator


class SuperAdminAddForm(UserBaseAddForm):
    class Meta(UserBaseForm.Meta):
        model = SuperAdmin


class SuperAdminEditForm(UserBaseEditForm):
    class Meta(UserBaseForm.Meta):
        model = SuperAdmin


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
