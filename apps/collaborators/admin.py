from django.contrib import admin

# Register your models here.
from apps.collaborators.forms import CompanyForm
from apps.collaborators.models import Food, Drink, Medicine, Other
from apps.collaborators.utilities.constants import FOOD_ID, DRINK_ID, MEDICINE_ID, OTHER_ID
from core.admin import _build_preview, AuditAdmin
from core.constants import PREVIEW


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', '_icon_preview')
    list_display_links = list_display
    fieldsets = [('Datos básicos', {
        'fields': ('name', ('icon', '_icon_preview'),)
    }), ] + AuditAdmin.fieldsets
    readonly_fields = ('_icon_preview',) + AuditAdmin.readonly_fields
    form = CompanyForm

    def _icon_preview(self, obj):
        return _build_preview(obj.pk, obj.icon)

    _icon_preview.short_description = PREVIEW


@admin.register(Food)
class FoodAdmin(CompanyAdmin):
    def save_model(self, request, obj, form, change):
        obj.type = FOOD_ID
        super(__class__, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(type=FOOD_ID)


@admin.register(Drink)
class DrinkAdmin(CompanyAdmin):
    def save_model(self, request, obj, form, change):
        obj.type = DRINK_ID
        super(__class__, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(type=DRINK_ID)


@admin.register(Medicine)
class MedicineAdmin(CompanyAdmin):
    def save_model(self, request, obj, form, change):
        obj.type = MEDICINE_ID
        super(__class__, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(type=MEDICINE_ID)


@admin.register(Other)
class OtherAdmin(CompanyAdmin):
    def save_model(self, request, obj, form, change):
        obj.type = OTHER_ID
        super(__class__, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(__class__, self).get_queryset(request)
        return qs.filter(type=OTHER_ID)
