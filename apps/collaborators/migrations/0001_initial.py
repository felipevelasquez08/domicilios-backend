# Generated by Django 3.1.3 on 2020-11-06 03:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre de la compañía')),
            ],
            options={
                'verbose_name': 'Compañía',
                'verbose_name_plural': 'Compañias',
            },
        ),
    ]
