from django import forms
from django.forms import ModelForm

from apps.collaborators.models import Company
from core.constants import NAME


class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': NAME})
        }
