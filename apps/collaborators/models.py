from django.db import models

# Create your models here.
from apps.collaborators.utilities.constants import COMPANY, COMPANIES, UPLOAD_TO, TYPE_CHOICES, TYPE, OTHER_ID, FOOD, \
    FOODS, DRINK, DRINKS, MEDICINE, MEDICINES, OTHER, OTHERS
from core.constants import NAME, ICON
from core.models import Audit


class Company(Audit):
    name = models.CharField(verbose_name=NAME, max_length=150, )
    icon = models.ImageField(verbose_name=ICON, upload_to=UPLOAD_TO)
    type = models.SmallIntegerField(verbose_name=TYPE, choices=TYPE_CHOICES, default=OTHER_ID)

    class Meta:
        verbose_name = COMPANY
        verbose_name_plural = COMPANIES

    def __str__(self):
        return f'{self.name}'


class Food(Company):
    class Meta:
        verbose_name = FOOD
        verbose_name_plural = FOODS
        proxy = True


class Drink(Company):
    class Meta:
        verbose_name = DRINK
        verbose_name_plural = DRINKS
        proxy = True


class Medicine(Company):
    class Meta:
        verbose_name = MEDICINE
        verbose_name_plural = MEDICINES
        proxy = True


class Other(Company):
    class Meta:
        verbose_name = OTHER
        verbose_name_plural = OTHERS
        proxy = True
