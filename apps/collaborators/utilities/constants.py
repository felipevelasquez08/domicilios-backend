# Company

TYPE = 'Tipo de colaborador'
FOOD_ID = 101
DRINK_ID = 102
MEDICINE_ID = 103
OTHER_ID = 100
TYPE_CHOICES = (
    (FOOD_ID, 'Comidas'),
    (DRINK_ID, 'Bebidas'),
    (MEDICINE_ID, 'Medicamentos'),
    (OTHER_ID, 'Otros')
)

COMPANY = 'Compañía'
COMPANIES = 'Compañias'
UPLOAD_TO = 'company/icon'

# Food

FOOD = 'Comida'
FOODS = 'Comidas'

# Drink

DRINK = 'Bebida'
DRINKS = 'Bebidas'

# Medicine

MEDICINE = 'Medicamentos'
MEDICINES = 'Medicamentos'

# Other

OTHER = 'Otro'
OTHERS = 'Otros'
