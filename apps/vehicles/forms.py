from django import forms

from apps.vehicles.models import Brand, Model
from apps.vehicles.utilities.constants import BRAND
from core.constants import NAME


class BrandForm(forms.ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': NAME})
        }


class ModelForm(forms.ModelForm):
    brand = forms.ModelChoiceField(label=BRAND, queryset=Brand.objects.all())

    class Meta:
        model = Model
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': NAME})
        }
