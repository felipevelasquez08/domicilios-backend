# Generated by Django 3.1.3 on 2020-11-11 01:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0004_auto_20201110_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brand',
            name='name',
            field=models.CharField(max_length=150, verbose_name='Nombre'),
        ),
    ]
