from django.db import models

# Create your models here.
from apps.vehicles.utilities.constants import BRANDS, BRAND, UPLOAD_TO, MODEL, MODELS
from core.constants import NAME, ICON
from core.models import Audit


class Brand(Audit):
    name = models.CharField(verbose_name=NAME, max_length=150, )
    icon = models.ImageField(verbose_name=ICON, upload_to=UPLOAD_TO)

    class Meta:
        verbose_name = BRAND
        verbose_name_plural = BRANDS

    def __str__(self):
        return self.name


class Model(Audit):
    name = models.CharField(verbose_name=NAME, max_length=150, )
    brand = models.ForeignKey(Brand, verbose_name='Marca del vehículo', max_length=50, on_delete=models.CASCADE, )

    class Meta:
        verbose_name = MODEL
        verbose_name_plural = MODELS

    def __str__(self):
        return f'{self.name} ({self.brand})'
