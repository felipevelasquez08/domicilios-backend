from django.contrib import admin

from apps.vehicles.forms import BrandForm, ModelForm
from apps.vehicles.models import Brand, Model
from core.admin import _build_preview, AuditAdmin
from core.constants import PREVIEW


# Register your models here.


@admin.register(Brand)
class BrandAdmin(AuditAdmin):
    list_display = ('name', '_icon_preview')
    list_display_link = list_display
    fieldsets = [('Datos básicos', {
        'fields': ('name', ('icon', '_icon_preview'))
    }), ] + AuditAdmin.fieldsets
    readonly_fields = ('_icon_preview',) + AuditAdmin.readonly_fields
    form = BrandForm

    def _icon_preview(self, obj):
        return _build_preview(obj.pk, obj.icon)

    _icon_preview.short_description = PREVIEW
    _icon_preview.admin_order_field = 'name'


@admin.register(Model)
class ModelAdmin(AuditAdmin):
    list_display = ('name', 'brand', '_brand_icon_preview')
    list_display_link = list_display
    fieldsets = [('Datos básicos', {
        'fields': ('name', ('brand', '_brand_icon_preview'))
    }), ] + AuditAdmin.fieldsets
    readonly_fields = ('_brand_icon_preview',)
    form = ModelForm

    def _brand_icon_preview(self, obj):
        return _build_preview(obj.pk, obj.brand.icon)

    _brand_icon_preview.short_description = PREVIEW
    _brand_icon_preview.admin_order_field = 'name'
