from datetime import datetime

from django.db import models

from apps.collaborators.models import Company
from apps.orders.utilities.constants import COST, ORDER_COST, DESCRIPTION, DELIVERY_COURIER, FINISH_DATE, \
    FINISH, CANCEL, COMPANY, ORDER, ORDERS
from apps.users.models import User
# Create your models here.
from core.models import Audit


class Tracing(Audit):
    finish_date = models.DateTimeField(verbose_name=FINISH_DATE, null=True, blank=True)
    finish = models.BooleanField(verbose_name=FINISH, default=False)
    cancel = models.BooleanField(verbose_name=CANCEL, default=False)

    class Meta:
        abstract = True


class Order(Tracing):
    cost = models.IntegerField(verbose_name=COST)
    cost_order = models.IntegerField(verbose_name=ORDER_COST)
    description = models.TextField(verbose_name=DESCRIPTION)
    delivery_courier = models.ForeignKey(User, verbose_name=DELIVERY_COURIER, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, verbose_name=COMPANY, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name = ORDER
        verbose_name_plural = ORDERS

    @staticmethod
    def format_price(obj):
        return f'${obj}'

    @property
    def format_cost(self):
        return self.format_price(self.cost)

    @property
    def format_cost_order(self):
        return self.format_price(self.cost_order)

    def __str__(self):
        return f'Orden número {self.id} del mensajero {self.delivery_courier}'

    def save(self, *args, **kwargs):
        if self.finish or self.cancel:
            if self.finish_date is None:
                self.finish_date = datetime.now()
        super(__class__, self).save(*args, **kwargs)

