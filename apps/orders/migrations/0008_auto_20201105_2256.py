# Generated by Django 3.1.3 on 2020-11-06 03:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0007_order_cancel'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': 'Orden', 'verbose_name_plural': 'Ordenes'},
        ),
        migrations.AlterField(
            model_name='order',
            name='active',
            field=models.BooleanField(default=True, verbose_name='¿Está activo?'),
        ),
        migrations.AlterField(
            model_name='order',
            name='cancel',
            field=models.BooleanField(default=False, verbose_name='¿Está cancelado?'),
        ),
        migrations.AlterField(
            model_name='order',
            name='cost',
            field=models.IntegerField(verbose_name='Costo'),
        ),
        migrations.AlterField(
            model_name='order',
            name='cost_order',
            field=models.IntegerField(verbose_name='Costo de la orden'),
        ),
        migrations.AlterField(
            model_name='order',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación'),
        ),
        migrations.AlterField(
            model_name='order',
            name='description',
            field=models.TextField(verbose_name='Descripción'),
        ),
        migrations.AlterField(
            model_name='order',
            name='finish',
            field=models.BooleanField(default=False, verbose_name='¿Ya finalizó?'),
        ),
        migrations.AlterField(
            model_name='order',
            name='finish_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Fecha de finalización'),
        ),
        migrations.AlterField(
            model_name='order',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Domiciliario'),
        ),
    ]
