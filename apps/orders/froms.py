from django import forms

from apps.collaborators.models import Company
from apps.orders.models import Order
from apps.orders.utilities.constants import COMPANY, COST, ORDER_COST, DELIVERY_COURIER
from apps.orders.utilities.exceptions import finish_and_cancel
from apps.users.models import User


class OrderForm(forms.ModelForm):
    cost = forms.IntegerField(label=COST)
    cost_order = forms.IntegerField(label=ORDER_COST)
    delivery_courier = forms.ModelChoiceField(
        label=DELIVERY_COURIER, queryset=User.objects.filter(is_superuser=False, is_admin=False, is_active=True)
    )
    company = forms.ModelChoiceField(label=COMPANY, queryset=Company.objects.all(), required=False, blank=True)

    class Meta:
        model = Order
        fields = '__all__'

    def clean(self):
        finish = self.cleaned_data.get('finish')
        cancel = self.cleaned_data.get('cancel')
        if finish and cancel:
            raise finish_and_cancel()


class OrderAddForm(OrderForm):
    pass


class OrderEditForm(OrderForm):
    pass
