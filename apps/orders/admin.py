from django.contrib import admin

from apps.orders.froms import OrderAddForm, OrderEditForm
from apps.orders.models import Order
# Register your models here.
from apps.orders.utilities.constants import COST, ORDER_COST
from core.admin import AuditAdmin


@admin.register(Order)
class OrderAdmin(AuditAdmin):
    list_display = (
        '_format_cost', '_format_cost_order', 'description', 'company', 'delivery_courier', 'creation_date', 'finish',
        'cancel')
    list_display_links = list_display
    search_fields = ('description',)
    list_filter = ('creation_date', 'cancel', 'finish', 'delivery_courier',)
    change_list_template = 'admin/change_list.html'
    date_hierarchy = 'creation_date'
    fieldsets = [
                    ('Datos básicos', {
                        'fields': (('cost', 'cost_order'), 'description', 'company'),
                    }),
                    ('Datos domicilio', {
                        'fields': ('delivery_courier', ('finish', 'cancel',), 'finish_date')
                    }),
                ] + AuditAdmin.fieldsets
    radio_fields = {'company': admin.HORIZONTAL}
    readonly_fields = ('finish_date',) + AuditAdmin.readonly_fields

    def _format_cost(self, obj):
        return obj.format_cost

    _format_cost.short_description = COST
    _format_cost.admin_order_field = 'cost'

    def _format_cost_order(self, obj):
        return obj.format_cost

    _format_cost_order.short_description = ORDER_COST
    _format_cost_order.admin_order_field = 'cost_order'

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(__class__, self).get_readonly_fields(request, obj)
        if obj:
            if obj.finish:
                readonly_fields += ('cancel',)
            elif obj.cancel:
                readonly_fields += ('finish',)
        return readonly_fields

    def get_form(self, request, obj=None, change=False, **kwargs):
        return OrderAddForm if obj is None else OrderEditForm
