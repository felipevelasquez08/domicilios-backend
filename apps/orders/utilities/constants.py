# ORDER

COST = 'Costo'
ORDER_COST = 'Costo de la orden'
DESCRIPTION = 'Descripción'
DELIVERY_COURIER = 'Domiciliario'
FINISH_DATE = 'Fecha de finalización'
FINISH = '¿Ya finalizó?'
FINISH_KEY = 'finish'
CANCEL = '¿Está cancelado?'
CANCEL_KEY = 'cancel'
COMPANY = 'Proveedor'

ORDER = 'Orden'
ORDERS = 'Ordenes'
