from django import forms


def finish_and_cancel():
    return forms.ValidationError(
        'Lo sentimos, pero no puedes finalizar y cancelar la orden, debes escoger una de las dos')
