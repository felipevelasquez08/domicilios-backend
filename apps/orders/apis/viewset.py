from rest_framework import viewsets, response, status

from apps.orders.apis.serializers import OrderSerializer
from apps.orders.utilities.constants import FINISH_KEY, CANCEL_KEY


def validate_serializer(data, instance=None):
    serializer = OrderSerializer(data=data, instance=instance, partial=True)
    serializer.is_valid(raise_exception=True)
    serializer.save()


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    _is_authenticated = {'deatil': 'No recibimos ninguna credencial. Lo sentimos :('}

    def list(self, request, *args, **kwargs):
        delivery_courier = request.user
        if not delivery_courier.is_anonymous:
            data = self.get_queryset()
            serializer = OrderSerializer(data, many=True, context={'request', request})
            return response.Response(serializer.data)
        else:
            return response.Response(self._is_authenticated)

    def create(self, request, *args, **kwargs):
        delivery_courier = request.user
        if not delivery_courier.is_anonymous:
            data_create = request.data
            data_create['delivery_courier'] = delivery_courier.id
            validate_serializer(data=data_create)
            data_orders = self.get_queryset()
            serializer_orders = OrderSerializer(data_orders, many=True, context={'request': request})
            return response.Response(serializer_orders.data)
        else:
            return response.Response(self._is_authenticated)

    def update(self, request, *args, **kwargs):
        delivery_courier = request.user
        if not delivery_courier.is_anonymous:
            instance = self.get_object()
            data_update = request.data
            validate_serializer(data=data_update, instance=instance)
            data_orders = self.get_queryset()
            serializer_orders = OrderSerializer(data_orders, many=True, context={'request': request})
            return response.Response(serializer_orders.data, status=status.HTTP_200_OK)
        else:
            return response.Response(self._is_authenticated)

    def get_queryset(self):
        data = self.request.user.order_set.filter(active=True).order_by('-creation_date')
        state = self.request.query_params.get('state', None)
        if state is not None:
            if state == FINISH_KEY:
                data = data.filter(finish=True)
            elif state == CANCEL_KEY:
                data = data.filter(cancel=True)
        return data
